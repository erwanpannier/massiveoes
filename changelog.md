2018-04-23 Merge branch 'measuredSpectra_as_dict'
-------------------------------------------------

The massiveOES.MeasuredSpectra internal structure was substantially changed.
Newly, the data are stored in the attribute spectra, which is an OrderedDict.
Each entry has a key provided by the user - an identificator. The key points
to a small dictionary 'params' and 'spectrum', where 'params' points to
massiveOES.Parameters object and 'spectrum' holds the data.

short example:
```
my_spectra = OrderedDict()
for key in keys:
    my_spectra[key] = {'spectrum': some_data_as_1D_numpy_array}

#during init, each spectrum gets its 'params'
m = MeasuredSpectra(spectra = my_spectra)

#before this was m.spectra[my_index1][1]
#new:
spec1 = m.spectra['my_key1']['spectrum'] 

#before this was m.spectra.parameters_list[my_index]
#new:
params1 = m.spectra['my_key1']['params']
```
