# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 14:52:47 2015

@author: Petr
"""

import numpy as np
from massiveOES import spectrum, Spectrum, MeasuredSpectra

sim = np.load('1000_1000.npy')
ssim = Spectrum(x=sim[:,0],y=sim[:,1])


real = MeasuredSpectra.from_FHRfile('ICCD_ar_profil_80Hz_1.txt',[])
sreal = real.measured_Spectra_for_fit(0)

step = np.abs(np.mean(np.diff(sreal.x)))
ssim.convolve_with_slit_function(gauss = 1e-3, lorentz=1e-3, step = step)

spectrum.compare_spectra(sreal,ssim,show_it=True)
