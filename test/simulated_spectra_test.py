from massiveOES import Spectrum, SimulatedSpectra #bere se ze souboru spectrum.py
from pylab import *


### nadefinuje uzivatel
#data = load('cesta/k/mym/datum')
#data = load('C:\Users\Petr\Downloads\800_1000.npy')
n2 = SimulatedSpectra.load('/home/jan/packages/massiveOES/test/N2CB.pkl')

n2.open_db('../massiveOES/data/N2CB.db')
tms = n2.get_temperatures()
Trot1 = n2.rotations[-8]
Tvib = n2.vibrations[-3]

s_from_mesh1 = n2.interpolate_spectra(Trot = Trot1, 
                                     Tvib = Tvib, 
                                     wmin=334, 
                                     wmax=338)
#s_from_mesh1.refine_mesh()



Trot2 = n2.rotations[-7]
Tvib = n2.vibrations[-3]

s_from_mesh2 = n2.interpolate_spectra(Trot = Trot2, 
                                     Tvib = Tvib, 
                                     wmin=334, 
                                     wmax=338)
#s_from_mesh2.refine_mesh()

s_from_2 = n2.interpolate_spectra((Trot1+Trot2)/2 , Tvib, wmin=334, wmax=338)



#s_from_2.refine_mesh()

plot(s_from_mesh1.x, s_from_mesh1.y)
plot(s_from_mesh2.x, s_from_mesh2.y)
plot(s_from_2.x, s_from_2.y)
show()
