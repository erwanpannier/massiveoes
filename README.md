# MassiveOES #

Welcome to MassiveOES homepage. MassiveOES is software developed specifically for the analysis of optical emission spectra of plasma discharges, including **batch processing**. Our goal is to focus both experimental and scientific challenges which you will encounter during plasma spectroscopy. The python library is developed by Dr. Jan Vorac and Dr. Petr Synek of Masaryk University in Brno, [CEPLANT centre](http://www.ceplant.cz/). 

### If this software helps you with a scientific publication, please cite our articles! As the development of this program is not binded to any specific funding the time and resources we devote to it comes from another projects or our free time. 

* Voráč, J., Kusýn, L., Synek, P. (2019)
 Deducing rotational quantum-state distributions from overlapping molecular spectra. [*Review of Scientific Instruments* 90, 123102](https://doi.org/10.1063/1.5128455)

* Voráč, J., Synek, P., Potočňáková, L., Hnilica, J., & Kudrle, V. (2017).
 Batch processing of overlapping molecular spectra as a tool for spatio-temporal diagnostics of power modulated microwave plasma jet. [*Plasma Sources Science and Technology*, 26(2), 025010.](https://doi.org/10.1088/1361-6595/aa51f0)

* Voráč, J., Synek, P., Procházka, V., & Hoder, T., (2017). 
  State-by-state emission spectra fitting for non-equilibrium plasmas: OH spectra of surface barrier discharge at argon/water interface. [*Journal of Physics D: Applied Physics*, 50(29), 294002 ](https://doi.org/10.1088/1361-6463/aa7570). 

**The current official user interface is based on [jupyter notebook](https://jupyter.org/) and can be found in the file massiveOES_interface.ipynb. To use it, follow the instructions in the file and execute one cell after another.**

Click here for the (outdated) standalone compiled versions for [Windows 7](http://physics.muni.cz/~janvorac/massiveOES_build_for_windows7.zip) and for [Linux](http://physics.muni.cz/~janvorac/massiveOES_linux_x64.zip).
Due to some incompatibility of pyinstaler and scipy libraries current precompiled version is out of date and with known absolute intensity issues! These issues are already solved in code so if you need to work with absolute intensities go the hard way (it's not so hard).

Please note that the spectral simulations have photon flux as their y-axis (photons/s/m2), not the intensity (J/s/m2). For narrow spectral windows, good results may be obtained without spectral sensitivity calibration. If you wish to perform the calibration anyway (yes, it is a good practice), mind the units!

We are not authors of any spectral database included. Please, see the file **data_sources.txt** in the directory **data** for sources of spectral data and **cite them appropriately**. 

The software is divided to a computational part, that can be easily used for scripting and the graphical user interface (GUI), which provides a convenient way to use most of the core program functions. The authors are aware that the GUI is not bug-free and not every event is handled properly. However, the level of **support dealing the GUI bugs will be strongly limited** by the time capabilities of the authors. Much higher priority from our side will be payed to eventual errors in the core program, as this is supposed to be our main contribution to the scientific community.

We strongly advise the users of the GUI to check the text output in the console opened every time with the main window as most of the eventual errors are not reported in the GUI.

Also, we still see things that need to be improved in the internal file structure. It is therfore quite likely, at this stage, that the files created by the program could not be read properly by newer versions. Therefore, always keep the source data! 

We have added some basic video guides for GUI

[massiveOES GUI guide part #1 - Basic operation](https://youtu.be/Asu8-pUEMG0)

[massiveOES GUI guide part #2 - batch processing](https://youtu.be/nFYS5KQe1Ks)

[massiveOES GUI guide part #3 - boltzman plot feature](https://youtu.be/fqhKtg-mH6E)

## Installation ##

If the compiled version (see above) is not enough for your needs, you can install the library. Clone the repository to a local directory and run

```
#!bash 
python setup.py develop
```

This will create links in your local python directory.

For details see [here](https://bitbucket.org/OES_muni/massiveoes/wiki/Home).


## Looking for solutions in atomic spectroscopy? ##
Try [Spectrum Analyzer](http://www.physics.muni.cz/~zdenek/span/) developed by our colleague Dr. Zdeněk Navrátil.